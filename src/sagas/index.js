export default function* rootSaga() {
    yield all([
        fooSagas,
        barSagas
    ])
}